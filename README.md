**Ardoise** est l’outil de répartition du chiffre d’affaires entre les associés de Codeurs en Liberté. 

## Développer en local

1. Installer ruby (de préférence avec [rbenv](https://github.com/rbenv/rbenv#installation)),
2. `make install` pour installer les dépendances,
3. Générer les tokens OAuth:
  * Créer une application "Ardoise" dans GitLab (`Profile Settings ▸ Applications`),
  * Copier les tokens générés dans `config/local_env.yml`.
4. `make run` pour lancer le serveur.

## Tester

_TODO_

## Déployer

_TODO_
