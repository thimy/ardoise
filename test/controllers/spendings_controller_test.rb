require 'test_helper'

class SpendingsControllerTest < ActionDispatch::IntegrationTest

  test "should create Spending" do
    associate = associates(:one)
    assert_difference('Spending.count', 1) do
      post spendings_url, params: {
        spending: {
          amount: 100,
          associate_id: associate.id,
        }
      }
    end

    assert_equal 100_00, Spending.last.amount

    assert_redirected_to associate_url(associate.id)
  end

  test "should destroy Spending" do
    assert_difference('Spending.count', -1) do
      delete spending_url(spendings(:one))
    end

    assert_redirected_to associate_url(associates(:one).id)
  end
end
