require 'test_helper'

class ShareModelTest < ActiveSupport::TestCase
  test "should share 0 with no bucket" do
    share_model = ShareModel.new
    assert_equal 0, share_model.compute_contribution(1000)
  end

  test "should share a fixed amount" do
    share_model = share_models(:flat)
    assert_equal 100, share_model.compute_contribution(1_000)
  end

  test "should return a negative amount" do
    share_model = share_models(:standard)
    assert_equal -500, share_model.compute_contribution(10_000)
  end

  test "should combine both brackets" do
    share_model = share_models(:standard)
    assert_equal 3_000_00, share_model.compute_contribution(60_000_00)
  end
end
