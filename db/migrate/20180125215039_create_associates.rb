class CreateAssociates < ActiveRecord::Migration[5.1]
  def change
    create_table :associates do |t|
      t.text :display_name
      t.text :email

      t.timestamps
    end
  end
end
