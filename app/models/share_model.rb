class ShareModel < ApplicationRecord
  has_many :share_model_buckets
  MAX_INT = 2 ** 62

  def compute_contribution(amount)
    # We sort from the highest bucket to the smallest
    buckets = self.share_model_buckets.sort { |a, b| b.lower_bound  <=> a.lower_bound }

    result = buckets.inject({contribution: 0, upper_bound: MAX_INT}) do |result, bucket|
      if amount > bucket.lower_bound
        to_split = [result[:upper_bound], amount].min - bucket.lower_bound
        contribution = result[:contribution] + to_split * bucket.rate

        {contribution: contribution, upper_bound: bucket.lower_bound}
      else
        result
      end
    end

    result[:contribution]
  end
end
