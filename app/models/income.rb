class Income < ApplicationRecord
  belongs_to :associate
  belongs_to :share_model

  def pretty_print
    "%0.2f (%s)" % [self.amount / 100, self.label]
  end
end
