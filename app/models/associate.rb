require './app/services/associate'

class Associate < ApplicationRecord
  has_many :incomes
  has_many :spendings
  has_many :salaries

  class << self
    def from_omniauth(auth_hash)
      first_associate = all.size == 0
      find_or_create_by(oauth_uid: auth_hash['uid'], oauth_provider: auth_hash['provider']) do |associate|
        # The first one to connect gets access
        # The following need to get the access from someone else
        if first_associate then
          associate.access = true
        end
        associate.display_name = auth_hash['info']['name']
        associate.email = auth_hash['info']['email']
        associate.save!
      end
    end
  end

  def data_by_month
    AssociateService.data_by_month(self)
  end

  def summary
    AssociateSummary.new(self.incomes, self.salaries, self.spendings)
  end
end
