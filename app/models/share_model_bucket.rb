class ShareModelBucket < ApplicationRecord
  belongs_to :share_model

  def lower_bound_euros
    lower_bound / 100
  end
end
