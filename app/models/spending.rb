class Spending < ApplicationRecord
  belongs_to :associate

  def pretty_print
    "%0.2f €" % [self.amount / 100]
  end
end
