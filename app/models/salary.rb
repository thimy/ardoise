class Salary < ApplicationRecord
  belongs_to :associate

  def pretty_print
    "Salaire direct : %0.2f € + %0.2f € (abondement), salaire socialisé : %0.2f € (salariales) + %0.2f € (patronales)" %
      [self.net_pay / 100, self.saving_plan_contribution / 100,
      self.employee_contribution / 100, self.employer_contribution / 100]
  end
end
