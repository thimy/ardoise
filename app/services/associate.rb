class AssociateService
  def self.data_by_month(associate)
    data = {}
    associate.spendings.all.each do |spending|
      month = [spending.year, spending.month]
      data[month] ||= {}
      data[month][:spending] ||= []
      data[month][:spending] << spending
    end

    associate.incomes.all.each do |income|
      month = [income.year, income.month]
      data[month] ||= {}
      data[month][:income] ||= []
      data[month][:income] << income
    end

    associate.salaries.all.each do |salary|
      month = [salary.year, salary.month]
      data[month] ||= {}
      data[month][:salary] ||= []
      data[month][:salary] << salary
    end

    data.sort{|a, b| b <=> a}.to_h
  end

  def self.total_contribution(all_incomes)
    contribution = 0

    all_incomes.group_by(&:year).each do |year, incomes_by_year|
      incomes_by_year.group_by(&:share_model_id).each do |share_model_id, incomes|
        nb_months = incomes.group_by(&:month).size
        share_model = ShareModel.find(share_model_id)
        total = incomes.map(&:amount).sum

        # We extrapolate the income if it was over a year
        extrapolated = total * 12 / nb_months
        contribution += share_model.compute_contribution(extrapolated) * nb_months / 12
      end
    end

    contribution
  end
end

class AssociateSummary
  BASE_SALARY = 1562
  SUPER_GROSS_TO_GROSS = 0.70
  attr_reader :contributions, :net_pay, :employee_contribution, :employer_contribution, :saving_plan_contribution, :spendings, :incomes

  def initialize(all_incomes, all_salaries, all_spendings)
    @contributions            = AssociateService.total_contribution(all_incomes) / 100.0
    @net_pay                  = all_salaries.map(&:net_pay).compact.sum / 100.0
    @employee_contribution    = all_salaries.map(&:employee_contribution).compact.sum / 100.0
    @employer_contribution    = all_salaries.map(&:employer_contribution).compact.sum / 100.0
    @saving_plan_contribution = all_salaries.map(&:saving_plan_contribution).compact.sum / 100.0
    @spendings                = all_spendings.map(&:amount).compact.sum / 100.0
    @incomes                  = all_incomes.map(&:amount).sum / 100.0
  end

  def net_balance
    @incomes -
      @contributions -
      @net_pay - @employee_contribution - @employer_contribution - @saving_plan_contribution -
      @spendings
  end

  def suggested_bonus
    AssociateSummary.compute_bonus(net_balance)
  end

  def self.compute_bonus(balance)
    if balance > 0
      balance * SUPER_GROSS_TO_GROSS - BASE_SALARY
    else
      0
    end
  end
end
